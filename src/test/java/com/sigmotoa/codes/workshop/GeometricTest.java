package com.sigmotoa.codes.workshop;

import org.junit.Test;

import static org.junit.Assert.*;

public class GeometricTest {


    @Test
    public void testSquareArea1() {
        assertSame(16, Geometric.squareArea(4));
    }
    @Test
    public void testSquareArea2() {
        assertSame(15, Geometric.squareArea(3,5));
    }
    @Test
    public void testSquareArea3() {
        assertSame(16, Geometric.squareArea(4,4));
    }
    @Test
    public void testSquareArea4() {
        assertEquals(IllegalArgumentException.class, Geometric.squareArea(-2));
    }
    @Test
    public void testSquareArea5() {
        assertEquals(IllegalArgumentException.class, Geometric.squareArea(-2,-2));
    }

    @Test
    public void testCircleArea1() { assertEquals(28.27, Geometric.circleArea(3.0),0.01);
    }
    @Test
    public void testCircleArea2() { assertEquals(0.0, Geometric.circleArea(0.0) ,0.01);
    }

    @Test
    public void testCircleArea3() {
        assertEquals(IllegalArgumentException.class, Geometric.circleArea(-3.0));
    }
    @Test
    public void testCirclePerimeter() {
        assertEquals(21.99, Geometric.circlePerimeter(7),0.01);
    }
    @Test
    public void testSquarePerimeter() {
        assertEquals(81.0, Geometric.squarePerimeter(9.0),0);
    }

    @Test
    public void testSphereVolume1() { assertEquals(14137.1, Geometric.sphereVolume(15.0),1);
    }

    @Test
    public void testSphereVolume2() {
        assertEquals(4188.79, Geometric.sphereVolume(10.0),1);
    }

    @Test
    public void testPentagonArea() { assertEquals(84.30339050292969, Geometric.pentagonArea(7),0.0);
    }

    @Test
    public void calculateHypotenuse() {
        assertEquals(5.0, Geometric.calculateHypotenuse(3.0,4.0),0);
        assertEquals(5.0, Geometric.calculateHypotenuse(4.0,3.0),0);

    }
}