package com.sigmotoa.codes.workshop;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConvertionTest {

    @Test
    public void kmTom() {
        assertEquals(12452.0,Convertion.kmTom(12.452),0.0);
    }

    @Test
    public void testKmTom() {
        assertEquals(178,Convertion.kmToM1(0.178));
    }

    @Test
    public void kmTocm() {
        assertEquals(7630,Convertion.kmTocm(0.07630),0.000000000001);
        assertEquals(5907000,Convertion.kmTocm(59.07),0.0000000000001);
    }

    @Test
    public void mmTom() {
        assertEquals(3141.516,Convertion.mmTom(3141516),0.1);
    }

    @Test
    public void milesToFoot() {
        assertEquals(469920.0,Convertion.milesToFoot(89.0),0);
    }

    @Test
    public void yardToInch() {
        assertEquals(3204,Convertion.yardToInch(89),0);
    }

    @Test
    public void inchToMiles() {
        assertEquals(0.0014678030303030302,Convertion.inchToMiles(93.0),0.0);
        assertEquals(1.4046717171717172E-5,Convertion.inchToMiles(0.89),0.0);

    }

    @Test
    public void footToYard() {
        assertEquals(316,Convertion.footToYard(950));
    }

    @Test
    public void kmToInch() {
        assertEquals(3748425.2,Convertion.kmToInch("95.21"),0.1);
    }

    @Test
    public void mmToFoot() {
        assertEquals(0.3123687,Convertion.mmToFoot("95.21"),0.1);
    }

    @Test
    public void yardToCm() {
        assertEquals(8706.0024,Convertion.yardToCm("95.21"),0.1);
    }

    @Test
    public void errors()
    {
        assertEquals("Error", true, Convertion.kmToInch("Km"));
        assertEquals("Error", true, Convertion.mmToFoot("mm"));
        assertEquals("Error", true, Convertion.yardToCm("yard"));
    }
}