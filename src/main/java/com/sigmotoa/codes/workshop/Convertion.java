package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static int kmToM1(double km)
    {
        double result = km*1000;
        int operation = (int)result;
        return  operation;
    }

//Km to metters
    public static double kmTom(double km)
    {
        double result = km*1000;
        return  result;
    }
    
    //Km to cm
    public static double kmTocm(double km)
    {
        double result = km*100000;
        return  result;
    }

//milimetters to metters
    public static double mmTom(int mm)
    {
        double result = mm*0.001;
        return  result;
    }
//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles)
    {
        double result = miles*5280;
        return result;
    }

//convert yards to inches
    public static int yardToInch(int yard)
    {
       int result = yard*36;
       return result;
    }
    
    //convert inches to miles
    public static double inchToMiles(double inch)
    {
        double result = inch/63360;
        return result;
    }
//convert foot to yards
    public static int footToYard(int foot)
    {
        int result = foot/3;
        return result;
    }

//Convert units in both systems

//convert Km to inches
    public static double kmToInch(String km)
    {
        double operation =Double.parseDouble(km);
        double result = operation*39370.079;
        return result;
    }

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {
        double operation =Double.parseDouble(mm);
        double result = operation/304.8;
        return result;
    }
//convert yards to cm    
    public static double yardToCm(String yard)
    {
        double operation =Double.parseDouble(yard);
        double result = operation*91.44;
        return result;
    }


}
