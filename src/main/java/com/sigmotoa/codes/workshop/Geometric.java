package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */




import jdk.jshell.spi.ExecutionControlProvider;

import static java.lang.Math.*;

public class Geometric {

//Calculate the area for a square using one side
public static int squareArea(int side)
{
    if(side<0){
        throw new IllegalArgumentException("Valor dado no valido");
    } else{
        int area = (int)Math.pow(side, 2);
        return area;
    }
}

//Calculate the area for a square using two sides
public static int squareArea(int sidea, int sideb) 
{
    if(sidea<0 || sideb<0 ){
        throw new IllegalArgumentException("Valor no valido");
    }
    else
        {
        int area = sidea * sideb;
        return area;
    }
}

//Calculate the area of circle with the radius
public static double circleArea(double radius) {
    if (radius < 0) {
        throw new IllegalArgumentException("Valor dado no valido");
    } else {
        double area = Math.pow(radius, 2) * Math.PI;
        return area;
    }
}

//Calculate the perimeter of square with a side
public static double squarePerimeter(double side)
{
    if(side < 0)
    {
        throw new IllegalArgumentException();
    }
    else {
        double perimeter = Math.pow(side, 2);
        return perimeter;
    }
}
    //Calculate the perimeter of circle with the diameter
    public static double circlePerimeter(int diameter)
    {

        double perimeter = Math.PI*diameter;
        return perimeter;
    }

//Calculate the volume of the sphere with the radius
public static double sphereVolume(double radius)
{
        double volume = (4 * Math.PI * Math.pow(radius, 3)/3);
        return volume;
}

//Calculate the area of regular pentagon with one side
public static float pentagonArea(int side)
{
    float area = (float) ((5*Math.pow(side, 2))/(4*Math.tan(Math.PI/5)));
    return area;
}

//Calculate the Hypotenuse with two cathetus
public static double calculateHypotenuse(double catA, double catB)
{
    double hypotenuse = Math.sqrt( Math.pow(catA, 2)+ Math.pow(catB, 2));
    return hypotenuse;
}
}
