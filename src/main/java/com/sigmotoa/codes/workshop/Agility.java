package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */
public class Agility {

    //bigger than

    //Show if the first number is bigger than the second
    public static boolean biggerThan(String numA, String numB)
    {
        double val1 = Double.parseDouble(numA);
        double val2 = Double.parseDouble(numB);
         if(val1>val2){
             return true;
         }
         else
             {
             return false;
         }
    }

    //Sort from bigger the numbers an show in array
    public static int[] order(int numA, int numB, int numC,int numD,int numE)
    {
        int[] numbers=new int[5];
        numbers[0]=numA;
        numbers[1]=numB;
        numbers[2]=numC;
        numbers[3]=numD;
        numbers[4]=numE;

        int i, j, aux;
        for (i = 0; i < numbers.length - 1; i++) {
            for (j = 0; j < numbers.length - i - 1; j++) {
                if (numbers[j + 1] < numbers[j]) {
                    aux = numbers[j + 1];
                    numbers[j + 1] = numbers[j];
                    numbers[j] = aux;
                }
            }
        }
        return numbers;
    }

    //Look for the smaller number of the array
    public static double smallerThan(double array []) {

        double menor, tmp;
        int i, j, pos;
        for (i = 0; i < array.length - 1; i++) {
            menor = array[i];
            pos = i; //
            for (j = i + 1; j < array.length; j++) {
                if (array[j] < menor) {
                    menor = array[j];
                    pos = j;
                }
            }
            if (pos != i) {
                tmp = array[i];
                array[i] = array[pos];
                array[pos] = tmp;
            }
        }
        return array[0];
    }

    //Palindrome number is called in Spanish capicúa
    //The number is palindrome
    public static boolean palindromeNumber(Integer numA)
    {
        int aux, num,invert = 0;
        aux = numA;
        while (aux!=0){
            num = aux % 10;
            invert = invert * 10 + num;
            aux = aux / 10;
        }

        if(numA == invert){
            return true;
        }else{
           return false;
        }
    }



    //the word is palindrome
    public static boolean palindromeWord(String word)
    {
        String pal = "";
        for (int i=word.length()-1;i >= 0;i--){
            pal = pal + word.charAt(i);
        }
        if (word.equals(pal)){
            return true;
        }
      return false;
    }


    //Show the factorial number for the parameter
   public static int factorial(int numA)
   {
       int resultado=1;
       if (numA == 0) {
       return 0;

       }else{
           for (int i = 1; i <= numA; i++) {
               resultado *= i;
           }
       }
       return resultado;
   }

   //is the number odd
   public static boolean isOdd(byte numA)
   {
       if(numA%2 == 0){
            return false;
       }else{
           return true;
       }
   }

   //is the number prime
   public static boolean isPrimeNumber(int numA)
   {
       int i,contador=0;
       for(i=1; i<=numA; i++){
            if((numA % i) == 0){
                contador++;
            }
       }

       if(contador == 2)
       {
          return true;
       }else{
           return false;
       }
   }

   //is the number even
    public static boolean isEven(byte numA)
    {
        if(numA%2 == 0){
        return true;
    }else{
        return false;
    }
    }

    //is the number perfect
    public static boolean isPerfectNumber(int numA) {
        int i, sum=0;
        for (i = 1; i < numA; i++) {
            if (numA % i == 0) {
                sum = sum + i;
            }
        }
        if (sum == numA) {
            return true;
        } else {
            return false;

        }
    }

    //Return an array with the fibonacci sequence for the requested number
    public static int [] fibonacci(int numA)
    {
        int pre = 0, pos = 1, terminos, val;
        int[] vec = new int[numA+1];
        vec[numA] = pre;
        vec[numA-1] = pos;

       return new int[0];
    }

    //how many times the number is divided by 3
    public static int timesDividedByThree(int numA) {
        int suma = 3, contador = 0;
        if (numA < 3) {
            throw new IllegalArgumentException("El numero debe ser mayor a 3");
        }else if(numA ==3){
            return 1;
        }
        else  {
            while (numA >= suma) {
                suma = suma + 3;
                contador++;
            }
            return contador;
        }
    }

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)
    /**
     * If number is divided by 3, show fizz
     * If number is divided by 5, show buzz
     * If number is divided by 3 and 5, show fizzbuzz
     * in other cases, show the number
     */


    {
        String fizz = "Fizz";
        String buzz ="Buzz";
        String fizzbuzz ="FizzBuzz";
        String enteroString = Integer.toString(numA);

        if(numA%5 == 0 && numA%3 == 0){
            return fizzbuzz;
        } else if(numA%5 == 0){
            return buzz;
        } else if(numA%3 == 0){
            return fizz;
        } else {
            return enteroString;
        }
    }


}
